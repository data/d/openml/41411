# OpenML dataset: youtube

https://www.openml.org/d/41411

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Mixed Output Task. The YouTube personality dataset consists of a collection of behavorial features, speech transcriptions, and personality impression scores for a set of 404 YouTube vloggers that explicitly show themselves in front of the a webcam talking about a variety of topics including personal issues, politics, movies, books, etc. There is no content-related restriction and the language used in the videos is natural and diverse.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41411) of an [OpenML dataset](https://www.openml.org/d/41411). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41411/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41411/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41411/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

